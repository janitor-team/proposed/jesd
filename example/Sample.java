/* JEsd
 * Copyright (C) 1999 JCraft Inc.
 *  
 * Written by: 1999 ymnk
 *   
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
   
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

import com.jcraft.jesd.*;
import java.io.*;

public class Sample{

  Sample(String[] arg) throws java.io.IOException{
    int rate=JEsd.ESD_DEFAULT_RATE;
    int bits=JEsd.ESD_BITS16, channels=JEsd.ESD_STEREO;
    int mode=JEsd.ESD_STREAM, func=JEsd.ESD_PLAY;

    int sample_id=0, confirm_id=0, reget_sample_id=0;
    int cache_mode=0;

    String file=null;
    String host="localhost";
    int i=0;
    for(;i<arg.length;i++){
      if(arg[i].equals("-h")){
	System.out.println("usage: Sample [-s server] [-d [-b] [-m] [-r freq]] file");
	System.exit(0);
      }
      if(arg[i].equals("-s")){
	i++;
	host=arg[i];
	continue;
      }
      if(arg[i].equals("-b")){
	bits=JEsd.ESD_BITS8;
	continue;
      }
      if(arg[i].equals("-m")){
	channels=JEsd.ESD_MONO;
	continue;
      }
      if(arg[i].equals("-d")){
	cache_mode=1;
	continue;
      }
      /*
      if(arg[i].equals("-e")){
	cache_mode=2;
	continue;
      }
      */
      if(arg[i].equals("-r")){
	i++;
	try{rate=Integer.valueOf(arg[i]).intValue();}
	catch(Exception e){}
	continue;
      }
      break;
    }
    file=arg[i];

    JEsd jesd=null;

    if(cache_mode==1){
      FileInputStream fis=new FileInputStream(file);
      int format=bits|channels|mode|func;

      try{ jesd=new JEsd(host);}
      catch(Exception e){
	System.err.println(e);
	System.exit(-1);
      }

      sample_id=jesd.sample_cache(format, rate, 
				  (int)(new File(file).length()), file);
      System.out.println("sample id is "+sample_id);

      byte[] buf=new byte[JEsd.ESD_BUF_SIZE];
      int length;
      int total=0;
      while((length=fis.read(buf,0,JEsd.ESD_BUF_SIZE))>0){
	if(jesd.write(buf, 0, length)<0) return;
	else total+=length;
      }

      confirm_id=jesd.confirm_sample_cache();
      if(confirm_id!=sample_id){
	System.out.println("error while caching sample <"+sample_id+
			   ">: confirm returned "+confirm_id);
	System.exit(-1);
      }
      fis.close();
      System.out.println("sample <"+sample_id+"> upladed, "+total+" bytes");
    }
    else if (cache_mode==0) {
      try{jesd=new JEsd(host);}
      catch(Exception e){
	System.err.println(e);
	System.exit(-1);
      }
      sample_id=jesd.file_cache("sample", file);
      if(sample_id<0){
	System.out.println("error while caching sample <"+sample_id+
			   ">: confirm value != sample_id");
	System.exit(-1);
      }
      System.out.println("sample <"+sample_id+"> upladed: "+file);
    }
    else if (cache_mode==2){
    }
    
    while(true){
      System.out.println("press 'q' <enter> to quit, <enter> to trigger");
      int key=System.in.read();
      if('q'==(byte)key) break;
      jesd.sample_play(sample_id);
    }
    jesd.sample_free(sample_id);
    System.out.println("closing down\n");
    jesd.close();
  }

  public static void main(String[] arg){
    try{ new Sample(arg); }
    catch(Exception e){}
  }
}

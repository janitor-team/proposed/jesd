/* JEsd
 * Copyright (C) 1999 JCraft Inc.
 *  
 * Written by: 1999 ymnk
 *   
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
   
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.jcraft.jesd;

import java.net.*;
import java.io.*;

public class DaemonWithVorbis extends Thread {
  static{
    JEsd.installStreamPlayerPlugin("com.jcraft.jesd.vorbis.StreamPlayer");
    JEsd.installSamplePlugin("com.jcraft.jesd.vorbis.Sample");
  }

  static private int port=JEsd.ESD_DEFAULT_PORT;
  static private String key=null;

  static Object currentSound=null;

  private ServerSocket server=null;

  public DaemonWithVorbis(){
    Auth.init(key);
    try{ server = new ServerSocket(port); }
    catch (IOException e) {
      System.out.println(e);
    }
  }

  public void run(){
    if(server==null) return; // ??
    while(true){
      try {
	Socket socket = server.accept();
	Client client = new Client(socket);
	client.start();
      } 
      catch (IOException e) {
	//System.out.println(e);
      }
    }
  }

  public void setPort(int p){port=p;}
  public void setKey(String k){key=k;}

  static void protoServerInfo(IO io) throws java.io.IOException{
    int version=io.readInt();
    io.writeInt(100);    // version  // ??
    io.writeInt(44100);  // rate
    io.writeInt(0x1021); // format 
  }

  static void protoAllInfo(IO io) throws java.io.IOException{
    int version=io.readInt();
    io.writeInt(100);    // version  // ??
    io.writeInt(44100);  // rate
    io.writeInt(0x1021); // format

    // player
    Player.dumpInfo(io);
    io.writeInt(0); // source id
    io.writePad(JEsd.ESD_NAME_MAX+16);

    // sample
    Sample.dumpInfo(io);
    io.writeInt(0); // sample id
    io.writePad(JEsd.ESD_NAME_MAX+20);
  }

  public static void main(String args[]) {
    (new DaemonWithVorbis()).start();
  }
}
